/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2017 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.components.solr.utils;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.apache.solr.client.solrj.request.CollectionAdminRequest;
import org.apache.solr.client.solrj.request.CollectionAdminRequest.AddReplica;
import org.apache.solr.client.solrj.request.CollectionAdminRequest.Backup;
import org.apache.solr.client.solrj.request.CollectionAdminRequest.Create;
import org.apache.solr.client.solrj.request.CollectionAdminRequest.CreateShard;
import org.apache.solr.client.solrj.request.CollectionAdminRequest.Delete;
import org.apache.solr.client.solrj.request.CollectionAdminRequest.DeleteReplica;
import org.apache.solr.client.solrj.request.CollectionAdminRequest.DeleteShard;
import org.apache.solr.client.solrj.request.CollectionAdminRequest.Restore;
import org.apache.solr.client.solrj.request.CoreAdminRequest;
import org.apache.solr.client.solrj.request.CoreAdminRequest.Unload;
import org.apache.solr.client.solrj.request.SolrPing;
import org.apache.solr.client.solrj.response.CollectionAdminResponse;
import org.apache.solr.client.solrj.response.CoreAdminResponse;
import org.apache.solr.client.solrj.response.RequestStatusState;
import org.apache.solr.client.solrj.response.SolrPingResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.cloud.ClusterState;
import org.apache.solr.common.cloud.DocCollection;
import org.apache.solr.common.cloud.DocRouter;
import org.apache.solr.common.cloud.ImplicitDocRouter;
import org.apache.solr.common.cloud.Replica;
import org.apache.solr.common.cloud.Slice;
import org.apache.solr.common.cloud.ZkStateReader;
import org.apache.solr.common.params.CoreAdminParams;
import org.apache.solr.common.util.NamedList;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

/**
 * Provides some utilities to manage solr cloud (list / create / delete collections / shards...)
 *
 * @author lmartin
 */
public final class SolrUtils {


	/**
	 * Logger.
	 */
	private static final Log LOGGER = LogFactory.getLog(SolrUtils.class);


	private SolrUtils() {
		super();
	}


	/**
	 * Pinging SOLR collection. Warning this method just send a ping request to a collection (to check if collection exist). It does not check that all nodes
	 * are correctly running. To completely check that a solr collection is up with all its nodes OK, Use checkNodes method.
	 *
	 * @param solrClient
	 *            {@link SolrClient} solr client to ping
	 * @param collections
	 *            {@link String} list of collections to ping
	 * @return Whether or not the collections were all pinged
	 * @throws SolrServerException
	 *             if there is an error on the Solr server
	 * @throws IOException
	 *             if there is a communication error
	 */
	public static final boolean ping(final SolrClient solrClient, final String... collections) throws SolrServerException, IOException {
		final long startTime;
		final String collectionNames;
		if (SolrUtils.LOGGER.isDebugEnabled()) {
			final StringBuilder sb = new StringBuilder();
			for (final String collection : collections) {
				if (sb.length() > 0) {
					sb.append(", ");
				}
				sb.append(collection);
			}
			collectionNames = sb.toString();
			SolrUtils.LOGGER.debug("Pinging " + collectionNames + " index");
			startTime = System.currentTimeMillis();
		} else {
			startTime = 0L;
			collectionNames = "";
		}

		for (final String collection : collections) {

			final SolrPingResponse pingResponse = new SolrPing().process(solrClient, collection);

			if (SolrUtils.LOGGER.isDebugEnabled()) {
				SolrUtils.LOGGER.debug(collection + " ping response: " + pingResponse.getResponse());
			}

			if (pingResponse.getStatus() != 0) {
				final String errorMessage = collection + " ping error: " + pingResponse.getResponse();
				if (SolrUtils.LOGGER.isErrorEnabled()) {
					SolrUtils.LOGGER.error(errorMessage);
				}
				return false;
			}
		}

		if (SolrUtils.LOGGER.isDebugEnabled()) {
			SolrUtils.LOGGER.debug(collectionNames + " pinged in " + (System.currentTimeMillis() - startTime) + " ms");
		}
		return true;
	}


	/**
	 * Check that all leader nodes of a collection are correctly running
	 *
	 * @param client
	 *            {@link CloudSolrClient} clienty used to connect to solr
	 * @param collection
	 *            {@link String} collection to check
	 * @return boolean true if collection leaders are running correctly else false
	 */
	public static final boolean checkLeaders(final CloudSolrClient client, final String collection) {
		SolrUtils.checkZkConnection(client);
		final ClusterState state = client.getZkStateReader().getClusterState();
		if (state == null) {
			SolrUtils.LOGGER.error("Could not check Solr collection " + collection + ". ClusterState is null");
			return false;
		}
		// list of currently running nodes
		final ArrayList<String> liveNodes = new ArrayList<>();
		for (final String s : state.getLiveNodes()) {
			liveNodes.add(s);
		}
		// check that all leaders of this collection are in running nodes list
		final DocCollection docCollection = state.getCollection(collection);
		for (final Slice slice : docCollection.getSlices()) {
			final Replica replica = slice.getLeader();
			if (!liveNodes.contains(replica.getNodeName())) {
				SolrUtils.LOGGER.error(replica.getNodeName() + " is not live but is a leader. Solr collection " + collection + " is not in a correct state");
				return false;
			}
		}
		return true;
	}


	/**
	 * Optimise SOLR.
	 *
	 * @param solrClient
	 *            {@link SolrClient} current solr client used to connect to solr
	 * @param collection
	 *            The collection to optimise
	 * @return boolean true if optimisation was OK else false
	 * @throws IOException
	 *             If there is a low-level I/O error.
	 * @throws SolrServerException
	 *             if there is an error on the server
	 */
	public static final boolean optimize(final SolrClient solrClient, final String collection) throws SolrServerException, IOException {
		if (SolrUtils.LOGGER.isDebugEnabled()) {
			SolrUtils.LOGGER.debug("Optimising solr index");
		}

		final UpdateResponse optimiseResponse = solrClient.optimize(collection);

		if (SolrUtils.LOGGER.isDebugEnabled()) {
			SolrUtils.LOGGER.debug("Index optimised response: " + optimiseResponse.getResponse() + " in " + optimiseResponse.getQTime() + " ms");
		}

		return (optimiseResponse.getStatus() != 0);
	}


	/**
	 * Create a new solr collection
	 *
	 * @param solrClient
	 *            {@link CloudSolrClient} current solr client used to connect to solr
	 * @param collection
	 *            {@link String} solr collection name
	 * @param config
	 *            {@link String} the collection config
	 * @param shards
	 *            {@link String} commas separated list of inital shard to create
	 * @param routerField
	 *            {@link String} field used to route document in correct shard (in implicit router mode)
	 * @param numReplicas
	 *            int replication factor for this collection
	 * @param maxShardPerNode
	 *            int maximum number of shards per solr node
	 * @param timeout
	 *            int timeout to wait for this creation (in seconds)
	 * @return boolean true if request was completed else false
	 * @throws IOException
	 *             If there is a low-level I/O error.
	 * @throws SolrServerException
	 *             if there is an error on the server
	 * @throws InterruptedException
	 *             If an error occurs in the thread processing
	 */
	public static boolean createCollection(final CloudSolrClient solrClient, final String collection, final String config, final String shards, final String routerField, final int numReplicas,
			final int maxShardPerNode, final int timeout) throws IOException, SolrServerException, InterruptedException {

		final Create createCollection = CollectionAdminRequest.createCollection(collection, config, shards.split(",").length, numReplicas);
		createCollection.setShards(shards);
		if (StringUtils.hasText(routerField)) {
			createCollection.setRouterField(routerField);
			createCollection.setRouterName(ImplicitDocRouter.NAME);
		} else {
			createCollection.setRouterName(DocRouter.DEFAULT_NAME);
		}
		createCollection.setMaxShardsPerNode(Integer.valueOf(maxShardPerNode));
		final String collectionCreationId = "create_" + collection;
		final RequestStatusState response = createCollection.processAndWait(collectionCreationId, solrClient, timeout);
		return RequestStatusState.COMPLETED.equals(response);
	}


	/**
	 * backup a collection
	 *
	 * @param solrClient
	 *            {@link CloudSolrClient} current solr client used to connect to solr
	 * @param collection
	 *            {@link String} solr collection to backup
	 * @param location
	 *            {@link String} absolute path where to store backup on solr nodes (local)
	 * @param backupName
	 *            {@link String} name of the backup
	 * @param timeout
	 *            int timeout (in seconds) to wait for backup process.
	 * @return boolean true if backup succeeded else false
	 * @throws IOException
	 *             If there is a low-level I/O error.
	 * @throws SolrServerException
	 *             if there is an error on the server
	 * @throws InterruptedException
	 *             If an error occurs in the thread processing
	 */
	public static boolean backupCollection(final CloudSolrClient solrClient, final String collection, final String location, final String backupName, final int timeout)
			throws IOException, SolrServerException, InterruptedException {

		final Backup backup = CollectionAdminRequest.backupCollection(collection, backupName);
		backup.setLocation(location);
		final RequestStatusState state = backup.processAndWait(solrClient, timeout);
		return RequestStatusState.COMPLETED.equals(state);
	}


	/**
	 * Restore a collection from a backup
	 *
	 * @param solrClient
	 *            {@link CloudSolrClient} current solr client used to connect to solr
	 * @param collection
	 *            {@link String} solr collection to restore. This collection MUST not exist (use deleteCollection before)
	 * @param location
	 *            {@link String} absolute path where to find backup on solr nodes (local)
	 * @param backupName
	 *            {@link String} name of the backup
	 * @param maxShardsPerNode
	 *            int maximum number of shards per node (if &lt;0 then this property will no be used during restore process and all shards will go on
	 *            same node)
	 * @param timeout
	 *            int timeout (in seconds) to wait for restore process. If &lt; 0 then will wait infinitely. Else will return even if restore did not succeed
	 * @return boolean true if restore succeeded else false
	 * @throws IOException
	 *             If there is a low-level I/O error.
	 * @throws SolrServerException
	 *             if there is an error on the server
	 * @throws InterruptedException
	 *             If an error occurs in the thread processing
	 */
	public static boolean restoreCollection(final CloudSolrClient solrClient, final String collection, final String location, final String backupName, final int maxShardsPerNode, final int timeout)
			throws IOException, SolrServerException, InterruptedException {
		final Restore restore = CollectionAdminRequest.restoreCollection(collection, backupName);
		restore.setLocation(location);
		if (maxShardsPerNode > 0) {
			restore.setMaxShardsPerNode(maxShardsPerNode);
		}
		final RequestStatusState state = restore.processAndWait(solrClient, timeout);
		return RequestStatusState.COMPLETED.equals(state);
	}


	/**
	 * Delete a solr collection. Warning this operation could not be cancelled
	 *
	 * @param solrClient
	 *            {@link CloudSolrClient} current solr client used to connect to solr
	 * @param collection
	 *            {@link String} name of collection to delete
	 * @param timeout
	 *            int timeout to wait for this deletion (in seconds)
	 * @return boolean true if request was completed else false
	 * @throws IOException
	 *             If there is a low-level I/O error.
	 * @throws SolrServerException
	 *             if there is an error on the server
	 * @throws InterruptedException
	 *             If an error occurs in the thread processing
	 */
	public static boolean deleteCollection(final CloudSolrClient solrClient, final String collection, final int timeout) throws IOException, SolrServerException, InterruptedException {
		final Delete delete = CollectionAdminRequest.deleteCollection(collection);
		final RequestStatusState response = delete.processAndWait(solrClient, timeout);
		return RequestStatusState.COMPLETED.equals(response);
	}


	/**
	 * Get list of existing collections in solr
	 *
	 * @param solrClient
	 *            {@link CloudSolrClient} current solr client used to connect to current solr
	 * @return {@link List} whole list of existing collections of this solr
	 * @throws IOException
	 *             If there is a low-level I/O error.
	 * @throws SolrServerException
	 *             if there is an error on the server
	 */
	public static List<String> getCollections(final CloudSolrClient solrClient) throws SolrServerException, IOException {
		final org.apache.solr.client.solrj.request.CollectionAdminRequest.List list = new org.apache.solr.client.solrj.request.CollectionAdminRequest.List();
		final CollectionAdminResponse response = list.process(solrClient);
		final Map<String, NamedList<Integer>> collectionsStatus = response.getCollectionCoresStatus();
		final ArrayList<String> collections = new ArrayList<>();
		if (!CollectionUtils.isEmpty(collectionsStatus)) {
			collections.addAll(collectionsStatus.keySet());
		}
		return collections;
	}


	/**
	 * Create a new shard for a specific solr collection
	 *
	 * @param solrClient
	 *            {@link CloudSolrClient} current solr client used to connect to current solr
	 * @param collection
	 *            {@link String} solr collection name
	 * @param shard
	 *            {@link String} new shard name
	 * @param timeout
	 *            int timeout to wait for this creation (in seconds)
	 * @return boolean true if request was completed else false
	 * @throws IOException
	 *             If there is a low-level I/O error.
	 * @throws SolrServerException
	 *             if there is an error on the server
	 * @throws InterruptedException
	 *             If an error occurs in the thread processing
	 */
	public static boolean createShard(final CloudSolrClient solrClient, final String collection, final String shard, final int timeout) throws IOException, SolrServerException, InterruptedException {
		final CreateShard createShardRequest = CollectionAdminRequest.createShard(collection, shard);
		final String shardCreationId = collection + "_" + shard;
		final RequestStatusState response = createShardRequest.processAndWait(shardCreationId, solrClient, timeout);
		return RequestStatusState.COMPLETED.equals(response);
	}


	/**
	 * Get list of all shards of a solr collection
	 *
	 * @param solrClient
	 *            {@link CloudSolrClient} current solr client used to connect to current solr
	 * @param collection
	 *            {@link String} solr collection name
	 * @return {@link List} this collection shards
	 */
	public static final List<String> getShards(final CloudSolrClient solrClient, final String collection) {
		SolrUtils.checkZkConnection(solrClient);
		final DocCollection docCollection = solrClient.getZkStateReader().getClusterState().getCollection(collection);
		final ArrayList<String> shards = new ArrayList<>();
		if (docCollection != null) {
			final Collection<Slice> slices = docCollection.getSlices();
			if (!CollectionUtils.isEmpty(slices)) {
				for (final Slice slice : slices) {
					shards.add(slice.getName());
				}
			}
		}
		return shards;
	}


	/**
	 * Get list of all replicas of a solr collection. . A map with node name as key, replica names list as value
	 *
	 * @param solrClient
	 *            {@link CloudSolrClient} current solr client used to connect to current solr
	 * @param collection
	 *            {@link String} solr collection name
	 * @return {@link List} this collection shards
	 */
	public static final Map<String, List<String>> getReplicas(final CloudSolrClient solrClient, final String collection) {
		SolrUtils.checkZkConnection(solrClient);
		final DocCollection docCollection = solrClient.getZkStateReader().getClusterState().getCollection(collection);
		final HashMap<String, List<String>> replicasMap = new HashMap<>();
		if (docCollection != null) {
			final Collection<Slice> slices = docCollection.getSlices();
			if (!CollectionUtils.isEmpty(slices)) {
				for (final Slice slice : slices) {
					final Collection<Replica> replicas = slice.getReplicas();
					if (!CollectionUtils.isEmpty(replicas)) {
						for (final Replica replica : replicas) {
							final String nodeName = replica.getNodeName();
							final String replicaName = replica.getName();
							List<String> replicaNames = replicasMap.get(nodeName);
							if (replicaNames == null) {
								replicaNames = new ArrayList<>();
								replicasMap.put(nodeName, replicaNames);
							}
							replicaNames.add(replicaName);
						}
					}
				}
			}
		}
		return replicasMap;
	}


	/**
	 * Move a replica from one node to another
	 *
	 * @param solrClient
	 *            {@link CloudSolrClient} current solr client used to connect to current solr
	 * @param collection
	 *            {@link String} solr collection name
	 * @param shard
	 *            {@link String} shard name
	 * @param sourceNode
	 *            {@link String} node where this replica is currently running. Replica will be deleted from this node
	 * @param destNode
	 *            {@link String} new node for this replica. This node will become live node for this replica
	 * @param timeout
	 *            int timeout to wait for each operation (ie : replica creation then replica deletion) (in seconds)
	 * @return boolean true if replica was correctly moved else false
	 * @throws IOException
	 *             If there is a low-level I/O error.
	 * @throws SolrServerException
	 *             if there is an error on the server
	 * @throws InterruptedException
	 *             If an error occurs in the thread processing
	 */
	public static final boolean moveReplica(final CloudSolrClient solrClient, final String collection, final String shard, final String sourceNode, final String destNode, final int timeout)
			throws SolrServerException, InterruptedException, IOException {
		SolrUtils.checkZkConnection(solrClient);
		final ClusterState state = solrClient.getZkStateReader().getClusterState();
		if (state == null) {
			SolrUtils.LOGGER.error("Could not move replica collection=" + collection + ", shard=" + shard + ", node=" + sourceNode + ". ClusterState is null");
			return false;
		}

		// check that source replica exist
		final DocCollection docCollection = state.getCollection(collection);
		final Slice slice = docCollection.getSlice(shard);
		Replica sourceReplica = null;
		for (final Replica replica : slice.getReplicas()) {
			if (replica.getNodeName().equals(sourceNode)) {
				sourceReplica = replica;
				break;
			}
		}

		if (sourceReplica == null) {
			SolrUtils.LOGGER.error("Could not moveShard " + collection + "." + shard + ". ClusterState is null");
			return false;
		}

		// check that dest node exist and is live
		final ArrayList<String> liveNodes = new ArrayList<>();
		for (final String s : state.getLiveNodes()) {
			liveNodes.add(s);
		}
		if (!liveNodes.contains(destNode)) {
			SolrUtils.LOGGER.error("Could not move replica collection=" + collection + ", shard=" + shard + ", node=" + sourceNode + ". dest node " + destNode + " is not running");
			return false;
		}

		// add new replica using dest node
		final AddReplica request = CollectionAdminRequest.addReplicaToShard(collection, shard);
		request.setNode(destNode);
		request.processAndWait(solrClient, timeout);

		final DeleteReplica deleteRequest = CollectionAdminRequest.deleteReplica(collection, shard, sourceReplica.getName());
		deleteRequest.setDeleteDataDir(Boolean.TRUE);
		deleteRequest.setDeleteIndexDir(Boolean.TRUE);
		deleteRequest.setDeleteInstanceDir(Boolean.TRUE);
		deleteRequest.processAndWait(solrClient, timeout);
		return true;
	}


	/**
	 * get list of all live nodes
	 *
	 * @param client
	 *            {@link CloudSolrClient} current solr client used to connect to current solr
	 * @return {@link List} list of all live nodes
	 */
	public static final List<String> getLiveNodes(final CloudSolrClient client) {
		SolrUtils.checkZkConnection(client);
		final ClusterState state = client.getZkStateReader().getClusterState();
		if (state == null) {
			SolrUtils.LOGGER.error("Could not get live nodes. ClusterState is null");
			return null;
		}
		// list of currently running nodes
		final ArrayList<String> liveNodes = new ArrayList<>();
		for (final String s : state.getLiveNodes()) {
			liveNodes.add(s);
		}
		return liveNodes;
	}


	/**
	 * Upload a configuration file(s) into zookeeper ensemble bound to current solr client
	 *
	 * @param solrClient
	 *            {@link CloudSolrClient} current solr client used to connect to solr
	 * @param rootPath
	 *            {@link Path} local path to upload
	 * @param zkPath
	 *            {@link String} path on zookeeper server
	 * @param filenameExclusions
	 *            {@link Pattern} pattern of files to exclude
	 * @throws IOException
	 *             if there is a communication error
	 */
	public static final void uploadToZK(final CloudSolrClient solrClient, final Path rootPath, final String zkPath, final Pattern filenameExclusions) throws IOException {
		SolrUtils.checkZkConnection(solrClient);
		solrClient.getZkStateReader().getZkClient().uploadToZK(rootPath, zkPath, filenameExclusions);
	}


	/**
	 * Upload a configuration dir into zookeeper ensemble bound to current solr client
	 *
	 * @param solrClient
	 *            {@link CloudSolrClient} current solr client used to connect to current solr
	 * @param dir
	 *            {@link Path} local directory to upload
	 * @param configName
	 *            {@link String} name of uploaded solr configuration
	 * @throws IOException
	 *             if there is a communication error
	 */
	public static final void uploadConfigDir(final CloudSolrClient solrClient, final Path dir, final String configName) throws IOException {
		SolrUtils.checkZkConnection(solrClient);
		solrClient.getZkStateReader().getConfigManager().uploadConfigDir(dir, configName);
	}


	/**
	 * Unload a solr core.
	 *
	 * @param solrClient
	 *            {@link SolrClient} current solr client used to connect to solr
	 * @param coreName
	 *            {@link String} name of the core to unload
	 * @return boolean true if core was correctly unloaded else false
	 * @throws SolrServerException
	 *             if there is an error on the Solr server
	 * @throws IOException
	 *             if there is a communication error
	 */
	public static final boolean unloadCore(final SolrClient solrClient, final String coreName) throws SolrServerException, IOException {

		final Unload unload = new Unload(true);
		unload.setDeleteInstanceDir(true);
		unload.setDeleteDataDir(true);
		unload.setCoreName(coreName);

		final CoreAdminResponse unloadResponse = SolrUtils.doCoreAction(solrClient, unload);
		return (0 == unloadResponse.getStatus());
	}


	private static final CoreAdminResponse doCoreAction(final SolrClient solrClient, final CoreAdminRequest coreAdminRequest) throws SolrServerException, IOException {
		if (SolrUtils.LOGGER.isDebugEnabled()) {
			SolrUtils.LOGGER.debug(coreAdminRequest.getParams().get(CoreAdminParams.ACTION).toLowerCase() + "ing core : " + coreAdminRequest.getParams().get(CoreAdminParams.CORE));
		}

		final CoreAdminResponse adminResponse = coreAdminRequest.process(solrClient);

		if (SolrUtils.LOGGER.isDebugEnabled()) {
			SolrUtils.LOGGER.debug(coreAdminRequest.getParams().get(CoreAdminParams.ACTION).toLowerCase() + "ed core : " + coreAdminRequest.getParams().get(CoreAdminParams.CORE));
		}

		return adminResponse;
	}


	/**
	 * Delete a solr shard from a collection.
	 *
	 * @param solrClient
	 *            {@link SolrClient} current solr client used to connect to solr
	 * @param collectionName
	 *            {@link String} name of the collection
	 * @param shardName
	 *            {@link String} name of the shard to delete
	 * @throws IOException
	 *             If there is a low-level I/O error.
	 * @throws SolrServerException
	 *             if there is an error on the server
	 */
	public static final void deleteShard(final SolrClient solrClient, final String collectionName, final String shardName) throws SolrServerException, IOException {
		if (SolrUtils.LOGGER.isInfoEnabled()) {
			SolrUtils.LOGGER.info("Deleting shard '" + shardName + "' of collection '" + collectionName + "'.");
		}

		final DeleteShard deleteShard = CollectionAdminRequest.deleteShard(collectionName, shardName);
		deleteShard.process(solrClient);

		if (SolrUtils.LOGGER.isInfoEnabled()) {
			SolrUtils.LOGGER.info("Deleted shard '" + shardName + "' of collection '" + collectionName + "'.");
		}
	}


	/**
	 * check that CloudSolrClient is already connected to zookeeper ensemble else connect it
	 *
	 * @param client
	 *            {@link CloudSolrClient} client to check
	 */
	public static void checkZkConnection(final CloudSolrClient client) {
		synchronized (client) {
			final ZkStateReader reader = client.getZkStateReader();
			if (reader == null) {
				try {
					client.connect();
				} catch (final Exception e) {
					throw e;
				}
			}
		}
	}
}
